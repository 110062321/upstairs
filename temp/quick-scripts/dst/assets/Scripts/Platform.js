
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Platform.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a3e6dBne/JI4rscTjPZ8hyF', 'Platform');
// Scripts/Platform.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Platform = /** @class */ (function (_super) {
    __extends(Platform, _super);
    function Platform() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isTouched = false;
        _this.anim = null;
        _this.moveSpeed = 50;
        _this.camera = null;
        return _this;
        //
        // 2. There are two different types of platforms: "Normal" & Conveyor".
        //    For "Conveyor", you have to do "delivery effect" when player is in contact with it.
        //    Note that the platforms have "delivery effect" only when player stands on them. 
        //
        //    Hints: Change "linearVelocity" of the player's rigidbody to make him move.
        //    The move value is "moveSpeed".
        //
        // 3. All the platforms have only "upside" collision. You have to prevent the collisions from the other directions.
        //
        //    Hints: You can use "contact.getWorldManifold().normal" to judge collision direction.
        //
        // ================================================
    }
    Platform.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.camera = cc.find('Canvas/Main Camera');
        if (this.node.name == "Conveyor") {
            this.node.scaleX = (Math.random() >= 0.5) ? 1 : -1;
            this.moveSpeed *= this.node.scaleX;
        }
        else if (this.node.name == "Normal") {
            var canMove = (Math.random() > 0.8) ? true : false;
            if (canMove) {
                var moveDir = (Math.random() > 0.5) ? "v" : "h";
                var delayTime = Math.random() * 2;
                this.platformMove(moveDir, delayTime);
            }
        }
    };
    Platform.prototype.update = function (dt) {
        if (this.camera.y - this.node.y >= 190) // platform out of screen
            this.platformDestroy();
    };
    Platform.prototype.playAnim = function () {
        if (this.anim)
            this.anim.play();
    };
    Platform.prototype.platformDestroy = function () {
        this.node.destroy();
    };
    Platform.prototype.platformMove = function (moveDir, delayTime) {
        var easeRate = 2;
        // ===================== TODO =====================
        // 1. Make platform move back and forth. You should use moveDir to decide move direction.
        //    'v' for vertical, and 'h' for horizontal.
        // 2. Use action system to make platfrom move forever.
        //    For horizontal case, you should first move right 50 pixel in 2s and then move back to initial position in 2s
        //    For vertical case, you should first move up 50 pixel in 2s and then move back to initial position in 2s
        //    You need to use "easeInOut" to modify your action with "easeRate" as parameter.
        // 3. Use scheduleOnce with delayTime to run this action. 
        // ================================================
        if (moveDir == 'h') {
            var action = cc.repeatForever(cc.sequence(cc.moveBy(2, 50, 0).easing(cc.easeInOut(easeRate)), cc.moveBy(2, -50, 0).easing(cc.easeInOut(easeRate))));
            this.node.runAction(action);
        }
        if (moveDir == 'v') {
            var action = cc.repeatForever(cc.sequence(cc.moveBy(2, 0, 50).easing(cc.easeInOut(easeRate)), cc.moveBy(2, 0, -50).easing(cc.easeInOut(easeRate))));
            this.node.runAction(action);
        }
    };
    // ===================== TODO =====================
    // 1. In the physics lecture, we know that Cocos Creator
    //    provides four contact callbacks. You need to use callbacks to
    //    design different behaviors for different platforms.
    //
    //    Hints: The callbacks are "onBeginContact", "onEndContact", "onPreSolve", "onPostSolve".
    Platform.prototype.onBeginContact = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 1) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                var playerRigidBody = other.node.getComponent(cc.RigidBody);
                var currentVelocity = playerRigidBody.linearVelocity;
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, currentVelocity.y);
            }
        }
    };
    Platform.prototype.onEndContact = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 1) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                var playerRigidBody = other.node.getComponent(cc.RigidBody);
                var currentVelocity = playerRigidBody.linearVelocity;
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, currentVelocity.y);
            }
        }
    };
    Platform.prototype.onPreSolve = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 1) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                var playerRigidBody = other.node.getComponent(cc.RigidBody);
                var currentVelocity = playerRigidBody.linearVelocity;
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, currentVelocity.y);
            }
        }
    };
    Platform.prototype.onPostSolve = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 1) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                var playerRigidBody = other.node.getComponent(cc.RigidBody);
                var currentVelocity = playerRigidBody.linearVelocity;
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, currentVelocity.y);
            }
        }
    };
    Platform = __decorate([
        ccclass
    ], Platform);
    return Platform;
}(cc.Component));
exports.default = Platform;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcUGxhdGZvcm0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBZ0pDO1FBOUlhLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFFN0IsVUFBSSxHQUFpQixJQUFJLENBQUM7UUFFMUIsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUV2QixZQUFNLEdBQVksSUFBSSxDQUFDOztRQTJIakMsRUFBRTtRQUNGLHVFQUF1RTtRQUN2RSx5RkFBeUY7UUFDekYsc0ZBQXNGO1FBQ3RGLEVBQUU7UUFDRixnRkFBZ0Y7UUFDaEYsb0NBQW9DO1FBQ3BDLEVBQUU7UUFDRixtSEFBbUg7UUFDbkgsRUFBRTtRQUNGLDBGQUEwRjtRQUMxRixFQUFFO1FBQ0YsbURBQW1EO0lBQ3JELENBQUM7SUF0SUcsd0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFFNUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkQsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztTQUN0QzthQUNJLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksUUFBUSxFQUNsQztZQUNJLElBQUksT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNuRCxJQUFHLE9BQU8sRUFDVjtnQkFDSSxJQUFJLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7Z0JBQ2hELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQ3pDO1NBQ0o7SUFDTCxDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFPLEVBQUU7UUFFTCxJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSx5QkFBeUI7WUFDNUQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCwyQkFBUSxHQUFSO1FBQ0ksSUFBRyxJQUFJLENBQUMsSUFBSTtZQUNSLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVELGtDQUFlLEdBQWY7UUFFSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCwrQkFBWSxHQUFaLFVBQWEsT0FBZSxFQUFFLFNBQWlCO1FBRTNDLElBQUksUUFBUSxHQUFXLENBQUMsQ0FBQztRQUN6QixtREFBbUQ7UUFDbkQseUZBQXlGO1FBQ3pGLCtDQUErQztRQUMvQyxzREFBc0Q7UUFDdEQsa0hBQWtIO1FBQ2xILDZHQUE2RztRQUM3RyxxRkFBcUY7UUFDckYsMERBQTBEO1FBQzFELG1EQUFtRDtRQUNuRCxJQUFHLE9BQU8sSUFBRSxHQUFHLEVBQUM7WUFDWixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsYUFBYSxDQUN6QixFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FDbEgsQ0FBQztZQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9CO1FBQ0QsSUFBRyxPQUFPLElBQUUsR0FBRyxFQUFDO1lBQ1osSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLGFBQWEsQ0FDekIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQ2xILENBQUM7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMvQjtJQUNMLENBQUM7SUFFSCxtREFBbUQ7SUFDbkQsd0RBQXdEO0lBQ3hELG1FQUFtRTtJQUNuRSx5REFBeUQ7SUFDekQsRUFBRTtJQUNGLDZGQUE2RjtJQUMzRixpQ0FBYyxHQUFkLFVBQWUsT0FBTyxFQUFDLElBQUksRUFBQyxLQUFLO1FBQzdCLElBQUcsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFDdkMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM3QjthQUNHO1lBQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBRyxJQUFJLElBQUksVUFBVSxFQUFDO2dCQUNsQixJQUFJLGVBQWUsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzVELElBQUksZUFBZSxHQUFHLGVBQWUsQ0FBQyxjQUFjLENBQUM7Z0JBQ3JELEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuRztTQUNKO0lBQ0wsQ0FBQztJQUNELCtCQUFZLEdBQVosVUFBYSxPQUFPLEVBQUMsSUFBSSxFQUFDLEtBQUs7UUFDM0IsSUFBRyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBQztZQUN2QyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdCO2FBQ0c7WUFDQSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFHLElBQUksSUFBSSxVQUFVLEVBQUM7Z0JBQ2xCLElBQUksZUFBZSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxlQUFlLEdBQUcsZUFBZSxDQUFDLGNBQWMsQ0FBQztnQkFDckQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEY7U0FDSjtJQUNMLENBQUM7SUFDRCw2QkFBVSxHQUFWLFVBQVcsT0FBTyxFQUFDLElBQUksRUFBQyxLQUFLO1FBQ3pCLElBQUcsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFDdkMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM3QjthQUNHO1lBQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBRyxJQUFJLElBQUksVUFBVSxFQUFDO2dCQUNsQixJQUFJLGVBQWUsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzVELElBQUksZUFBZSxHQUFHLGVBQWUsQ0FBQyxjQUFjLENBQUM7Z0JBQ3JELEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuRztTQUNKO0lBQ0wsQ0FBQztJQUNELDhCQUFXLEdBQVgsVUFBWSxPQUFPLEVBQUMsSUFBSSxFQUFDLEtBQUs7UUFDMUIsSUFBRyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBQztZQUN2QyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdCO2FBQ0c7WUFDQSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFHLElBQUksSUFBSSxVQUFVLEVBQUM7Z0JBQ2xCLElBQUksZUFBZSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxlQUFlLEdBQUcsZUFBZSxDQUFDLGNBQWMsQ0FBQztnQkFDckQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEY7U0FDSjtJQUNMLENBQUM7SUFsSWdCLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FnSjVCO0lBQUQsZUFBQztDQWhKRCxBQWdKQyxDQWhKcUMsRUFBRSxDQUFDLFNBQVMsR0FnSmpEO2tCQWhKb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQbGF0Zm9ybSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgcHJvdGVjdGVkIGlzVG91Y2hlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHByaXZhdGUgYW5pbTogY2MuQW5pbWF0aW9uID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIG1vdmVTcGVlZDogbnVtYmVyID0gNTA7XHJcblxyXG4gICAgcHJpdmF0ZSBjYW1lcmE6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuYW5pbSA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbik7XHJcblxyXG4gICAgICAgIHRoaXMuY2FtZXJhID0gY2MuZmluZCgnQ2FudmFzL01haW4gQ2FtZXJhJyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIkNvbnZleW9yXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnNjYWxlWCA9IChNYXRoLnJhbmRvbSgpID49IDAuNSkgPyAxIDogLTE7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVNwZWVkICo9IHRoaXMubm9kZS5zY2FsZVg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYodGhpcy5ub2RlLm5hbWUgPT0gXCJOb3JtYWxcIilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGxldCBjYW5Nb3ZlID0gKE1hdGgucmFuZG9tKCkgPiAwLjgpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICBpZihjYW5Nb3ZlKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbW92ZURpciA9IChNYXRoLnJhbmRvbSgpID4gMC41KSA/IFwidlwiIDogXCJoXCI7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVsYXlUaW1lID0gTWF0aC5yYW5kb20oKSAqIDI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBsYXRmb3JtTW92ZShtb3ZlRGlyLCBkZWxheVRpbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdClcclxuICAgIHsgICBcclxuICAgICAgICBpZih0aGlzLmNhbWVyYS55IC0gdGhpcy5ub2RlLnkgPj0gMTkwKSAvLyBwbGF0Zm9ybSBvdXQgb2Ygc2NyZWVuXHJcbiAgICAgICAgICAgIHRoaXMucGxhdGZvcm1EZXN0cm95KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUFuaW0oKSB7XHJcbiAgICAgICAgaWYodGhpcy5hbmltKVxyXG4gICAgICAgICAgICB0aGlzLmFuaW0ucGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHBsYXRmb3JtRGVzdHJveSgpXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgIH1cclxuXHJcbiAgICBwbGF0Zm9ybU1vdmUobW92ZURpcjogc3RyaW5nLCBkZWxheVRpbWU6IG51bWJlcilcclxuICAgIHtcclxuICAgICAgICBsZXQgZWFzZVJhdGU6IG51bWJlciA9IDI7XHJcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09IFRPRE8gPT09PT09PT09PT09PT09PT09PT09XHJcbiAgICAgICAgLy8gMS4gTWFrZSBwbGF0Zm9ybSBtb3ZlIGJhY2sgYW5kIGZvcnRoLiBZb3Ugc2hvdWxkIHVzZSBtb3ZlRGlyIHRvIGRlY2lkZSBtb3ZlIGRpcmVjdGlvbi5cclxuICAgICAgICAvLyAgICAndicgZm9yIHZlcnRpY2FsLCBhbmQgJ2gnIGZvciBob3Jpem9udGFsLlxyXG4gICAgICAgIC8vIDIuIFVzZSBhY3Rpb24gc3lzdGVtIHRvIG1ha2UgcGxhdGZyb20gbW92ZSBmb3JldmVyLlxyXG4gICAgICAgIC8vICAgIEZvciBob3Jpem9udGFsIGNhc2UsIHlvdSBzaG91bGQgZmlyc3QgbW92ZSByaWdodCA1MCBwaXhlbCBpbiAycyBhbmQgdGhlbiBtb3ZlIGJhY2sgdG8gaW5pdGlhbCBwb3NpdGlvbiBpbiAyc1xyXG4gICAgICAgIC8vICAgIEZvciB2ZXJ0aWNhbCBjYXNlLCB5b3Ugc2hvdWxkIGZpcnN0IG1vdmUgdXAgNTAgcGl4ZWwgaW4gMnMgYW5kIHRoZW4gbW92ZSBiYWNrIHRvIGluaXRpYWwgcG9zaXRpb24gaW4gMnNcclxuICAgICAgICAvLyAgICBZb3UgbmVlZCB0byB1c2UgXCJlYXNlSW5PdXRcIiB0byBtb2RpZnkgeW91ciBhY3Rpb24gd2l0aCBcImVhc2VSYXRlXCIgYXMgcGFyYW1ldGVyLlxyXG4gICAgICAgIC8vIDMuIFVzZSBzY2hlZHVsZU9uY2Ugd2l0aCBkZWxheVRpbWUgdG8gcnVuIHRoaXMgYWN0aW9uLiBcclxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAgICAgICBpZihtb3ZlRGlyPT0naCcpe1xyXG4gICAgICAgICAgICBsZXQgYWN0aW9uID0gY2MucmVwZWF0Rm9yZXZlcihcclxuICAgICAgICAgICAgICAgIGNjLnNlcXVlbmNlKGNjLm1vdmVCeSgyLDUwLDApLmVhc2luZyhjYy5lYXNlSW5PdXQoZWFzZVJhdGUpKSxjYy5tb3ZlQnkoMiwtNTAsMCkuZWFzaW5nKGNjLmVhc2VJbk91dChlYXNlUmF0ZSkpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGFjdGlvbik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKG1vdmVEaXI9PSd2Jyl7XHJcbiAgICAgICAgICAgIGxldCBhY3Rpb24gPSBjYy5yZXBlYXRGb3JldmVyKFxyXG4gICAgICAgICAgICAgICAgY2Muc2VxdWVuY2UoY2MubW92ZUJ5KDIsMCw1MCkuZWFzaW5nKGNjLmVhc2VJbk91dChlYXNlUmF0ZSkpLGNjLm1vdmVCeSgyLDAsLTUwKS5lYXNpbmcoY2MuZWFzZUluT3V0KGVhc2VSYXRlKSkpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oYWN0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gIC8vID09PT09PT09PT09PT09PT09PT09PSBUT0RPID09PT09PT09PT09PT09PT09PT09PVxyXG4gIC8vIDEuIEluIHRoZSBwaHlzaWNzIGxlY3R1cmUsIHdlIGtub3cgdGhhdCBDb2NvcyBDcmVhdG9yXHJcbiAgLy8gICAgcHJvdmlkZXMgZm91ciBjb250YWN0IGNhbGxiYWNrcy4gWW91IG5lZWQgdG8gdXNlIGNhbGxiYWNrcyB0b1xyXG4gIC8vICAgIGRlc2lnbiBkaWZmZXJlbnQgYmVoYXZpb3JzIGZvciBkaWZmZXJlbnQgcGxhdGZvcm1zLlxyXG4gIC8vXHJcbiAgLy8gICAgSGludHM6IFRoZSBjYWxsYmFja3MgYXJlIFwib25CZWdpbkNvbnRhY3RcIiwgXCJvbkVuZENvbnRhY3RcIiwgXCJvblByZVNvbHZlXCIsIFwib25Qb3N0U29sdmVcIi5cclxuICAgIG9uQmVnaW5Db250YWN0KGNvbnRhY3Qsc2VsZixvdGhlcil7XHJcbiAgICAgICAgaWYoY29udGFjdC5nZXRXb3JsZE1hbmlmb2xkKCkubm9ybWFsLnkgPCAxKXtcclxuICAgICAgICAgICAgY29udGFjdC5zZXRFbmFibGVkKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgdmFyIG5hbWUgPSBzZWxmLm5vZGUubmFtZTtcclxuICAgICAgICAgICAgaWYobmFtZSA9PSBcIkNvbnZleW9yXCIpe1xyXG4gICAgICAgICAgICAgICAgdmFyIHBsYXllclJpZ2lkQm9keSA9IG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keSk7XHJcbiAgICAgICAgICAgICAgICB2YXIgY3VycmVudFZlbG9jaXR5ID0gcGxheWVyUmlnaWRCb2R5LmxpbmVhclZlbG9jaXR5O1xyXG4gICAgICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KS5saW5lYXJWZWxvY2l0eSA9IGNjLnYyKHRoaXMubW92ZVNwZWVkLCBjdXJyZW50VmVsb2NpdHkueSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBvbkVuZENvbnRhY3QoY29udGFjdCxzZWxmLG90aGVyKXtcclxuICAgICAgICBpZihjb250YWN0LmdldFdvcmxkTWFuaWZvbGQoKS5ub3JtYWwueSA8IDEpe1xyXG4gICAgICAgICAgICBjb250YWN0LnNldEVuYWJsZWQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICB2YXIgbmFtZSA9IHNlbGYubm9kZS5uYW1lO1xyXG4gICAgICAgICAgICBpZihuYW1lID09IFwiQ29udmV5b3JcIil7XHJcbiAgICAgICAgICAgICAgICB2YXIgcGxheWVyUmlnaWRCb2R5ID0gb3RoZXIubm9kZS5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KTtcclxuICAgICAgICAgICAgICAgIHZhciBjdXJyZW50VmVsb2NpdHkgPSBwbGF5ZXJSaWdpZEJvZHkubGluZWFyVmVsb2NpdHk7XHJcbiAgICAgICAgICAgICAgICBvdGhlci5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgY3VycmVudFZlbG9jaXR5LnkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgb25QcmVTb2x2ZShjb250YWN0LHNlbGYsb3RoZXIpe1xyXG4gICAgICAgIGlmKGNvbnRhY3QuZ2V0V29ybGRNYW5pZm9sZCgpLm5vcm1hbC55IDwgMSl7XHJcbiAgICAgICAgICAgIGNvbnRhY3Quc2V0RW5hYmxlZChmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHZhciBuYW1lID0gc2VsZi5ub2RlLm5hbWU7XHJcbiAgICAgICAgICAgIGlmKG5hbWUgPT0gXCJDb252ZXlvclwiKXtcclxuICAgICAgICAgICAgICAgIHZhciBwbGF5ZXJSaWdpZEJvZHkgPSBvdGhlci5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpO1xyXG4gICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRWZWxvY2l0eSA9IHBsYXllclJpZ2lkQm9keS5saW5lYXJWZWxvY2l0eTtcclxuICAgICAgICAgICAgICAgIG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keSkubGluZWFyVmVsb2NpdHkgPSBjYy52Mih0aGlzLm1vdmVTcGVlZCwgY3VycmVudFZlbG9jaXR5LnkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgb25Qb3N0U29sdmUoY29udGFjdCxzZWxmLG90aGVyKXtcclxuICAgICAgICBpZihjb250YWN0LmdldFdvcmxkTWFuaWZvbGQoKS5ub3JtYWwueSA8IDEpe1xyXG4gICAgICAgICAgICBjb250YWN0LnNldEVuYWJsZWQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICB2YXIgbmFtZSA9IHNlbGYubm9kZS5uYW1lO1xyXG4gICAgICAgICAgICBpZihuYW1lID09IFwiQ29udmV5b3JcIil7XHJcbiAgICAgICAgICAgICAgICB2YXIgcGxheWVyUmlnaWRCb2R5ID0gb3RoZXIubm9kZS5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KTtcclxuICAgICAgICAgICAgICAgIHZhciBjdXJyZW50VmVsb2NpdHkgPSBwbGF5ZXJSaWdpZEJvZHkubGluZWFyVmVsb2NpdHk7XHJcbiAgICAgICAgICAgICAgICBvdGhlci5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgY3VycmVudFZlbG9jaXR5LnkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gIC8vXHJcbiAgLy8gMi4gVGhlcmUgYXJlIHR3byBkaWZmZXJlbnQgdHlwZXMgb2YgcGxhdGZvcm1zOiBcIk5vcm1hbFwiICYgQ29udmV5b3JcIi5cclxuICAvLyAgICBGb3IgXCJDb252ZXlvclwiLCB5b3UgaGF2ZSB0byBkbyBcImRlbGl2ZXJ5IGVmZmVjdFwiIHdoZW4gcGxheWVyIGlzIGluIGNvbnRhY3Qgd2l0aCBpdC5cclxuICAvLyAgICBOb3RlIHRoYXQgdGhlIHBsYXRmb3JtcyBoYXZlIFwiZGVsaXZlcnkgZWZmZWN0XCIgb25seSB3aGVuIHBsYXllciBzdGFuZHMgb24gdGhlbS4gXHJcbiAgLy9cclxuICAvLyAgICBIaW50czogQ2hhbmdlIFwibGluZWFyVmVsb2NpdHlcIiBvZiB0aGUgcGxheWVyJ3MgcmlnaWRib2R5IHRvIG1ha2UgaGltIG1vdmUuXHJcbiAgLy8gICAgVGhlIG1vdmUgdmFsdWUgaXMgXCJtb3ZlU3BlZWRcIi5cclxuICAvL1xyXG4gIC8vIDMuIEFsbCB0aGUgcGxhdGZvcm1zIGhhdmUgb25seSBcInVwc2lkZVwiIGNvbGxpc2lvbi4gWW91IGhhdmUgdG8gcHJldmVudCB0aGUgY29sbGlzaW9ucyBmcm9tIHRoZSBvdGhlciBkaXJlY3Rpb25zLlxyXG4gIC8vXHJcbiAgLy8gICAgSGludHM6IFlvdSBjYW4gdXNlIFwiY29udGFjdC5nZXRXb3JsZE1hbmlmb2xkKCkubm9ybWFsXCIgdG8ganVkZ2UgY29sbGlzaW9uIGRpcmVjdGlvbi5cclxuICAvL1xyXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG59XHJcbiJdfQ==